#include <iostream>
#include <typeinfo>

using namespace std;

int x;
float y;
short s;
double z;
unsigned short k;

//=========================|ImplicitType/Неявное|==============================//
/*
==Числовое_расширение
-Интегральное
-С_плавающей_точкй

==Числовая_конверсия

==Обработка_арифметических_выражений
*/
void imType()
{
    cout << "Enter variable 'int x' : ";
    cin >> x;
    cout << "Enter variable 'float y' : ";
    cin >> y;
    cout << "Enter variable 'double z' : ";
    cin >> z;
    cout << "Enter variable 'unsigned short k' : ";
    cin >> k;
    printf("\n");
    cout << "=============ImplicitType=============" << endl;
    int i = (x + y)*z + k;
    cout << "1. int i = (x + y)*z + k = " << i << typeid(i).name() << endl;
    k = (x - y)/z;
    cout << "2. unsigned short k = (x - y)/z = " << k << typeid(k).name() << endl;
    z = x*y + z/x;
    cout << "3. double z = x*y + z/x = " << z << typeid(z).name() << endl;
    unsigned int ui = x/y + k;
    cout << "4. unsigned int ui = x/y + k = " << ui << typeid(ui).name() << endl;
//TODO: invalid operands of types?
  //  short s = x%(x/y);
  //  cout << "4. short s = x%(x/y) = " << s << typeid(s).name() << endl;
     printf("\n");

//=========================|ExplicaitType/Явное|==============================//

//==C-style cast
    cout << "=============ExplicaitType - C-style cast=============" << endl;
    {
    int i = (int)(x + y)*z + k;
    }
    cout << "1. int i = (int)(x + y)*z + k = " << i << typeid(i).name() << endl;
    {
    k = (unsigned short)(x - y)/z;
    }
    cout << "2. unsigned short k = (unsigned short)(x - y)/z = " << k << typeid(k).name() << endl;
    {
    z = (double)x*y + z/x;
    }
    cout << "3. double z = (double)x*y + z/x = " << z << typeid(z).name() << endl;
    {
    unsigned int ui = (unsigned int)x/y + k;
    }
    cout << "4. unsigned int ui = (unsigned int)x/y + k = " << ui << typeid(ui).name() << endl;
//TODO: invalid operands of types?
   // {
   // short s = (short)x%(x/y);
   // }
   // cout << "4. short s = (short)x%(x/y) = " << s << typeid(s).name() << endl;
    printf("\n");

//==static_cast
    cout << "=============ExplicaitType static_cast=============" << endl;

    i = static_cast<int>(x + y)*z + k;
    cout << "1. int i = static_cast<int>(x + y)*z + k = " << i << typeid(i).name() << endl;
    k = static_cast<unsigned short>(x - y)/z;
    cout << "2. unsigned short k = static_cast<unsigned short>(x - y)/z = " << k << typeid(k).name() << endl;
    z = static_cast<double>(x*y) + z/x;
    cout << "3. double z = static_cast<double>x*y + z/x = " << z << typeid(z).name() << endl;
    ui = static_cast<unsigned int>(x/y) + k;
    cout << "4. unsigned int ui = static_cast<unsigned int>x/y + k = " << ui << typeid(ui).name() << endl;
//TODO: invalid operands of types?
//    short s = static_cast<short>(x)%(x/y);
//    cout << "4. short s = static_cast<short>(x)%(x/y) = " << s << typeid(s).name() << endl;
    printf("\n");
//==ExplicaitType in ImplicitType
 //   system("pause");
}

