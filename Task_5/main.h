#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <cstdlib>

//=================|UNIT_1|=========================//
#define MAX(c, d)

int quantities();
inline int minInline(int a, int b);
//=================|UNIT_2|=========================//
int comparison();
//=================|UNIT_3|=========================//
void imType();
//=================|UNIT_4|=========================//
int calc();
#endif // MAIN_H
