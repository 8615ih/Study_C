#include <iostream>
#include "main.h"

/* Укажите необходимый номер UNIT-а:
 * 1 - max, min, average;
 * 2 - преобразование мм - см, м;
 * 3 - преобразование типов;
 * !(1,2,3) - калькулятор.
*/
#define UNIT 3

using namespace std;

int main()
{
    #if UNIT == 1
   quantities();
    //minInline(3, 4);
    #elif UNIT == 2
     comparison();
    #elif UNIT == 3
     imType();
    #else
        calc();
    #endif
   return 0;
}
