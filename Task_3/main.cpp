/*
* Лабораторная работа No3
* Исследование комментариев С++:
* This program just displays a string and exits
* Developer: Igor Khizhinsky
*/

/*============================|LIBRARIES|==============================================*/
#include <iostream> /* Подключаем библиотеку для ввода/вывода */
#include <QCoreApplication> /* Подключаем библиотеку QCoreApplication */
#include <stdio.h> /* Подключаем библиотеку для printf */
#include <iomanip> /* Подключаем библиотеку для манипуляторов */

using namespace std;

/*============================|VARIABLES|=============================================*/
// Глобальные переменные
double a;
char   n;
int    var(512);
// Прототипы функций trailing
auto intValue()  -> int;
auto boolValue() -> bool;
auto charValue() -> char;
auto longValue() -> long;
//double cube(double a);
//auto text(char n) -> char;
//char text(char n);

/*============================|FUNCTIONS|=============================================*/
// Вычисляем куб
double cube(double a)
{
    double result = a*a*a;
    return(result);
}

// Выводим char
char text(char n)
{
   int n1 = n;
   cout << "Ваше значение:" << endl;
   // TODO: ?????
   cout << "Код:" << n1 << endl;
   cout << "Символ:" << n << endl;
   return 0;
}

// Определение размеров основных типов
auto intValue()  -> int
{
   cout << "int: " << sizeof(intValue()) << " bytes;" << endl; // вывод длины в байтах типа int
   return 0;
}

auto boolValue() -> bool
{
   cout << "bool: " << sizeof(boolValue()) << " bytes;" << endl; // вывод длины в байтах типа bool
   return 0;
}

auto charValue() -> char
{
    cout << "Char: " << sizeof(charValue()) << " bytes;" << endl; // вывод длины в байтах типа char
    return 0;
}

auto longValue() -> long
{
    cout << "long: " << sizeof(longValue()) << " bytes;" << endl; // вывод длины в байтах типа long
    return 0;
}

// VOID
void printChar(int a='a', char b='b', char c='c', char d='d')
{
std::cout << "a: " << a << '\n';
std::cout << "b: " << b << '\n';
std::cout << "c: " << c << '\n';
std::cout << "d: " << d << '\n';
}

// Overload
int overLoad(int o, int l)
{
   cout << "int: " << sizeof(int()) << " bytes;" << endl;
   return 0;
}

float overLoad(float o, float l)
{
   cout << "int: " << sizeof(int()) << " bytes;" << endl;
   return 0;
}
/*============================|ENTRANCE|==============================================*/
int main() //Главная функция с которой начинается исполнение программы
{/* Закрытая скобка - начало блока стейтмена, кода относящийся к функции main */
    // Титульный лист (этикетка)
    cout << "*****\t\t\t\t   ТУСУР   \t\t\t\t*****" << endl;
    printf("\n");
    printf("\n");
    cout << setw(80) << right << "Faculty: Additional education" << endl;
    cout << setw(61) << right << "Group: 310" << endl;
    cout << setw(77) << right << "Developer: Igor Khizhinsky" << endl;
    cout << setw(69) << right << "Laboratory work: 3" << endl;
    printf("\n");
    cout << "\t\t\t\t   Томск 2023   \t\t\t\t" << endl;
    printf("\n");

/*=================================|Cube|===================================================*/
    cout << "\t\t\t****Вычисляем куб числа****\t\t\t" << endl;
    printf("\n");
    cout << "Введите число:" << endl;
    cin >> a;
    // double answer = cube(a);
    cout << "Ответ:" << cube(a) << endl;
    printf("\n");

/*=================================|Char|===================================================*/
    cout << "\t\t\t****Выводим char****\t\t\t" << endl;
    printf("\n");
    cout << "Введите символьное значение:" << endl;
    cin >> n;
    // TODO: ?????
    auto t = text(n);
    printf("\n");

/*=================================|Initialization|==========================================*/
    cout << "\t\t\t****Вывполняем инициализацию****\t\t\t" << endl;
    auto intSize = intValue();
    auto intBool = boolValue();
    auto intChar = charValue();
    auto intLong = longValue();
    printf("\n");

 /*=================================|Variables|==========================================*/
    cout << "\t\t\t****Global/Local variables****\t\t\t" << endl;
    int var(1024);
    cout << "Global var: " << ::var << "\n";
    cout << "Local var: " << var << "\n";
    printf("\n");

/*=================================|VOID|==========================================*/
    cout << "\t\t\t****VOID****\t\t\t" << endl;
    printChar();
    printf("\n");
    printChar(1);
    printf("\n");
    printChar(1, '%');
    printf("\n");
    printChar(1, '%', 'U');
    printf("\n");
    printChar(1, '%', 'U', '&');
    printf("\n");

/*=================================|OVERLOAD|==========================================*/
    cout << "\t\t\t****OVERLOAD****\t\t\t" << endl;
    int io(256), il(1024);
    cout << "Overload_1: " << sizeof(overLoad(io, il)) << " bytes;" << endl;
    float fo(256.32), fl(1024.56);
    cout << "Overload_2: " << sizeof(overLoad(fo, fl)) << " bytes;" << endl;

    return 0; // Возвращаемое значение
}/* Закрытая скобка - окончание блока стейтмена, кода относящийся к функции main */
/*================================================================================*/
// Heppy end!
/*================================================================================*/
