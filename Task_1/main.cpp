// 'Hello World!' program
/*
* Лабораторная работа No1
* Исследование комментариев С++:
* This program just displays a string and exits
* Developer: Igor Khizhinsky
*/

/*============================|LIBRARIES|==============================================*/
#include <iostream> /* Подключаем библиотеку для ввода/вывода */
// using namespace std;
/*============================|VARIABLES|=============================================*/
// Глобальные переменные


/*============================|ENTRANCE|==============================================*/

int main() //Главная функция с которой начинается исполнение программы
{/* Закрытая скобка - начало блока стейтмена, кода относящийся к функции main */
 // TODO: Тело функции
  std::cout << "Hello World!" << std::endl;
  std::cout << "Номер группы 310, факультет: Дополнительное образование и повышение квалификации в ТУСУРе" << std::endl;
  std::cout << "Разработчики программы: Хижинский Игорь Алексеевич" << std::endl;
  std::cout << "Номер Лабораторной работы: 1" << std::endl;
#if 0
    Directive comments
#endif
  return 0; // Возвращаемое значение
}/* Закрытая скобка - окончание блока стейтмена, кода относящийся к функции main */

/*================================================================================*/
// Heppy end!
/*================================================================================*/
