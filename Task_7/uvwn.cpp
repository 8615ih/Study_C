#include <algorithm>
#include <string>
#include <iostream>

using namespace std;

/*void swap(int &value1, int &value2)
{
    int temp = value1;
    value1 = value2;
    value2 = temp;
}


void rearrangeArray(int arr[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                swap(arr[j], arr[j + 1]);
            }
        }
    }
}


int uvwn()
{
int arr[] = {'U', 'V', 'W', 'N'};
int n = sizeof(arr) / sizeof(arr[0]);

rearrangeArray(arr, n);

std::cout << "Переставленный массив: ";
for (int i = 0; i < n; i++)
{
std::cout << static_cast<char>(arr[i]) << " ";
}

return 0;
}

*/



//============================================================================//
/*
void uvwn()
{
    std::string s = "UVWN";
    std::sort(s.begin(), s.end());
    do
    {
        std::cout << s << '\n';
    }
        while(std::next_permutation(s.begin(), s.end()));
}
*/





void permutations(string &str, int i, int n)
{
// базовое условие
    if (i == n - 1)
    {
        cout << str << endl;
        return;
    }

// обрабатываем каждый символ оставшейся строки
    for (int j = i; j < n; j++)
    {
// поменять местами символ с индексом `i` на текущий символ
        swap(str[i], str[j]);        // Используется STL `swap()`

// повторяется для подстроки `str[i+1, n-1]`
        permutations(str, i + 1, n);

// возврат (восстановление строки в исходное состояние)
        swap(str[i], str[j]);
    }
}

int uvwn()
{
    string strUVWM = "UVWN";
    string &str = strUVWM;

    permutations(str, 0, str.length());

    return 0;
}
