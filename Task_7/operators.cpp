#include <iostream>
#include <iomanip>

int operators()
{
int m = 300;
float FX = 300.600006;
char cht = 'z';

// выводим значения переменных
std::cout << "m = "<< m << '\n';
std::cout << "FX = " << std::setprecision(9) << FX << '\n';
std::cout << "cht = " << cht << '\n';
std::cout << std::endl;

// выводим значения адресов памяти переменных (& - оператор адреса)
std::cout << "Operator usage &:" << std::endl;
std::cout << "-----------------------" << std::endl;
std::cout << "Address m = "<< &m << '\n';
std::cout << "Address FX = " << &FX << '\n';
std::cout << "Address cht = " << static_cast<void*>(&cht) << '\n';
//or
//std::printf( "%p\n", &cht );
std::cout << std::endl;

// выводим значения ячеек памяти переменных (* - оператор разыменования)
std::cout << "Operator usage & and *:" << std::endl;
std::cout << "-----------------------" << std::endl;
std::cout << "Address m = "<< *&m << '\n';
std::cout << "Address FX = " << *&FX << '\n';
std::cout << "Address cht = " << *&cht << '\n';
std::cout << std::endl;

/* указатель - переменная, значением которой является адрес памяти
 * присваиваем значения адресов указателям
 */
int *pm = &m;      // указатель на значение
float *pFX = &FX;
char *pcht = &cht;

// выводим адреса указателей
std::cout << "Pointer variables only:" << std::endl;
std::cout << "-----------------------" << std::endl;
std::cout << "Address m = "<< pm << '\n';
std::cout << "Address FX = " << pFX << '\n';
std::cout << "Address cht = " << static_cast<void*>(pcht) << '\n';
//or
//std::printf( "%p\n", pcht );
std::cout << std::endl;

// разыменовываем
std::cout << "Pointer operators only:" << std::endl;
std::cout << "-----------------------" << std::endl;
std::cout << "Address m = "<< *pm << '\n';
std::cout << "Address FX = " << *pFX << '\n';
std::cout << "Address cht = " << *pcht << '\n';
std::cout << std::endl;

return 0;

}
