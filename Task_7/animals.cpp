#include <iostream>

enum class animals
    {
        PIG,
        CHICKEN,
        GOAT,
        CAT,
        DOG
    };

int getAnimalName(animals animal)
{
    switch (animal)
    {
    case animals::PIG:
        std::cout << "Pig\n";
        break;
    case animals::CHICKEN:
        std::cout << "Chicken\n";
        break;
    case animals::GOAT:
        std::cout << "Goat\n";
        break;
    case animals::CAT:
        std::cout << "Cat\n";
        break;
    case animals::DOG:
        std::cout << "Dog\n";
        break;
        default:
        std::cout << "Unknown\n";
        break;
    }
  return 0;
}

int printNumberOfLegs(animals animal)
{
    switch (animal)
    {
    case animals::PIG:
        std::cout << "has 4 paws\n";
        break;
    case animals::CHICKEN:
        std::cout << "has 2 paws\n";
        break;
    case animals::GOAT:
        std::cout << "has 4 paws\n";
        break;
    case animals::CAT:
        std::cout << "has 4 paws\n";
        break;
    case animals::DOG:
        std::cout << "has 4 paws\n";
        break;
        default:
        std::cout << "Unknown\n";
        break;
    }
  return 0;
}

int choiceAnimals()
    {
    std::cout << "ANIMALS" << std::endl;
    std::cout << "0 - Pig\n1 -Chicken\n2 - Goat\n3 - Cat\n4 - Dog " << std::endl;

    do
    {
        int animal;
        std::cout << "Choice animals: ";
        std::cin >> animal;
        getAnimalName(static_cast<animals>(animal));
        printNumberOfLegs(static_cast<animals>(animal));
    }
     while (true);

    return 0;
}


