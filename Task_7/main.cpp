#include <iostream>
#include "main.h"
#include <functional>

#define UNIT 6

int main()
{
// Указатели на функции
    std::function<int()> fcn1;
    std::function<int()> fcn2;
    std::function<int()> fcn3;
    std::function<int()> fcn4;
    std::function<int()> fcn5;
    std::function<int()> fcn6;

// aliases
    fcn1 = elevator;
    fcn2 = choiceAnimals;
    fcn3 = operators;
    fcn4 = array;
    fcn5 = uvwn;
    fcn6 = logic;

#if (UNIT == 1)
    fcn1();
#elif (UNIT == 2)
    fcn2();
#elif (UNIT == 3)
    fcn3();
#elif (UNIT == 4)
    fcn4();
#elif (UNIT == 5)
    fcn5();
#elif (UNIT == 6)
    fcn6();
#else
    std::cout << "Unknown" << std::endl;
#endif

    return 0;
}
