#include <iostream>

int array()
{
int ptr[5] = { 5, 7, 2, 9, 8 };

std::cout << "Element 0 has address: " << &ptr[0] << '\n';
std::cout << "Element 1 has address: " << &ptr[1] << '\n';
std::cout << "Element 2 has address: " << &ptr[2] << '\n';
std::cout << "Element 3 has address: " << &ptr[3] << '\n';
std::cout << "Element 4 has address: " << &ptr[4] << '\n';
std::cout << "===========================================" << std::endl;

int *ptr1 = &ptr[0];
int *ptr2 = &ptr[1];
int *ptr3 = &ptr[2];
int *ptr4 = &ptr[3];
int *ptr5 = &ptr[4];

std::cout << "Element 0 has value: " << *ptr1 << "\n";
std::cout << "Element 1 has value: " << *ptr2 << "\n";
std::cout << "Element 2 has value: " << *ptr3 << "\n";
std::cout << "Element 3 has value: " << *ptr4 << "\n";
std::cout << "Element 4 has value: " << *ptr5 << "\n";

return 0;
}
