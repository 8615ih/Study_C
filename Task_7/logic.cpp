#include <iostream>
#include <cmath>
#include <math.h>
#include <stdlib.h>

using namespace std;
/*
int binary(int int1, int int2)
{
    int bin1 = 0;
    int bin2 = 0;

for(int j=0; int1>0; j++)
    {
        bin1+=(int1%2)*pow(10.0,j);
        int1/=2;
    }
for(int j=0; int2>0; j++)
    {
        bin2+=(int2%2)*pow(10.0,j);
        int1/=2;
    }
int result = bin1 & bin2;
cout << "bin1: " << bin1 << endl;
cout << "bin2: " << bin2 << endl;
cout << result << endl;
return(0);
}
*/
int logic() {

    int choice;
    cout << "Выберите операцию: " << endl;
    cout << "1. Побитовое И" << endl;
    cout << "2. Побитовое ИЛИ" << endl;
    cin >> choice;

    switch (choice) {

        case 1:
            int int1, int2;
            cout << "Введите первое число: ";
            cin >> int1;
            cout << "Введите второе число: ";
            cin >> int2;
            cout << "Результат: " << (int1 & int2) << endl;
            break;
        case 2:
            int int3, int4;
            cout << "Введите первое число: ";
            cin >> int3;
            cout << "Введите второе число: ";
            cin >> int4;
            cout << "Результат: " << (int3 | int4) << endl;
            break;
        default:
            cout << "Неверный выбор операции." << endl;
            break;
    }

    return 0;
}
