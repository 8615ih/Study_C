#include <iostream>

enum level
    {
        LEVEL_0,
        LEVEL_1,
        LEVEL_2,
        LEVEL_3,
        LEVEL_4,
        LEVEL_5,
        LEVEL_6,
        LEVEL_7
    };
int enterFloor(level floor)
{
    switch (floor) {
    case LEVEL_0:
        std::cout <<"Ground floor\n";
        break;
    case LEVEL_1:
        std::cout <<"Reception floor\n";
        break;
    case LEVEL_2:
        std::cout <<"Retail space\n";
        break;
    case LEVEL_3:
        std::cout <<"Entertainment facilities\n";
        break;
    case LEVEL_4:
        std::cout <<"Service sector\n";
        break;
    case LEVEL_5:
        std::cout <<"Administration\n";
        break;
    case LEVEL_6:
        std::cout <<"Technical floor\n";
        break;
    case LEVEL_7:
        std::cout <<"Attic\n";
        break;
    default:
        std::cout << "Unknown\n";
        break;
    }
  return 0;
}

int elevator()
    {
    std::cout << "ELEVATOR" << std::endl;
    std::cout << "0 - Ground floor\n1 - Reception floor\n2 - Retail space\n3 - Entertainment facilities\n4 - Service sector "
                 "facilities\n5 - Administration\n6 - Technical floor\n7 - Attic " << std::endl;

    do
    {
        int floor;
        std::cout << "Enter floor: ";
        std::cin >> floor;
        enterFloor(static_cast<level>(floor));
    }
     while (true);

    return 0;
}
