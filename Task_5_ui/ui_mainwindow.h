/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *result;
    QPushButton *clear;
    QPushButton *btn_1;
    QPushButton *btn_2;
    QPushButton *btn_3;
    QPushButton *btn_4;
    QPushButton *btn_5;
    QPushButton *btn_6;
    QPushButton *btn_7;
    QPushButton *btn_8;
    QPushButton *btn_9;
    QPushButton *btn_0;
    QPushButton *btn_plus;
    QPushButton *btn_minus;
    QLineEdit *lineEdit;
    QPushButton *btn_multi;
    QPushButton *btn_divide;
    QPushButton *btn_dot;
    QPushButton *btn_and;
    QPushButton *btn_or;
    QPushButton *btn_xor;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(510, 308);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(200, 210, 81, 16));
        result = new QPushButton(centralWidget);
        result->setObjectName(QStringLiteral("result"));
        result->setGeometry(QRect(410, 130, 80, 111));
        QFont font;
        font.setPointSize(15);
        result->setFont(font);
        clear = new QPushButton(centralWidget);
        clear->setObjectName(QStringLiteral("clear"));
        clear->setGeometry(QRect(310, 170, 80, 71));
        clear->setFont(font);
        btn_1 = new QPushButton(centralWidget);
        btn_1->setObjectName(QStringLiteral("btn_1"));
        btn_1->setGeometry(QRect(10, 170, 80, 31));
        btn_2 = new QPushButton(centralWidget);
        btn_2->setObjectName(QStringLiteral("btn_2"));
        btn_2->setGeometry(QRect(100, 170, 80, 31));
        btn_3 = new QPushButton(centralWidget);
        btn_3->setObjectName(QStringLiteral("btn_3"));
        btn_3->setGeometry(QRect(190, 170, 80, 31));
        btn_4 = new QPushButton(centralWidget);
        btn_4->setObjectName(QStringLiteral("btn_4"));
        btn_4->setGeometry(QRect(10, 130, 80, 31));
        btn_5 = new QPushButton(centralWidget);
        btn_5->setObjectName(QStringLiteral("btn_5"));
        btn_5->setGeometry(QRect(100, 130, 80, 31));
        btn_6 = new QPushButton(centralWidget);
        btn_6->setObjectName(QStringLiteral("btn_6"));
        btn_6->setGeometry(QRect(190, 130, 80, 31));
        btn_7 = new QPushButton(centralWidget);
        btn_7->setObjectName(QStringLiteral("btn_7"));
        btn_7->setGeometry(QRect(10, 90, 80, 31));
        btn_8 = new QPushButton(centralWidget);
        btn_8->setObjectName(QStringLiteral("btn_8"));
        btn_8->setGeometry(QRect(100, 90, 80, 31));
        btn_9 = new QPushButton(centralWidget);
        btn_9->setObjectName(QStringLiteral("btn_9"));
        btn_9->setGeometry(QRect(190, 90, 80, 31));
        btn_0 = new QPushButton(centralWidget);
        btn_0->setObjectName(QStringLiteral("btn_0"));
        btn_0->setGeometry(QRect(100, 210, 80, 31));
        btn_plus = new QPushButton(centralWidget);
        btn_plus->setObjectName(QStringLiteral("btn_plus"));
        btn_plus->setGeometry(QRect(310, 130, 80, 31));
        btn_minus = new QPushButton(centralWidget);
        btn_minus->setObjectName(QStringLiteral("btn_minus"));
        btn_minus->setGeometry(QRect(310, 90, 80, 31));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(10, 10, 261, 61));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        lineEdit->setFont(font1);
        btn_multi = new QPushButton(centralWidget);
        btn_multi->setObjectName(QStringLiteral("btn_multi"));
        btn_multi->setGeometry(QRect(310, 50, 80, 31));
        btn_divide = new QPushButton(centralWidget);
        btn_divide->setObjectName(QStringLiteral("btn_divide"));
        btn_divide->setGeometry(QRect(310, 10, 80, 31));
        btn_dot = new QPushButton(centralWidget);
        btn_dot->setObjectName(QStringLiteral("btn_dot"));
        btn_dot->setGeometry(QRect(10, 210, 80, 31));
        btn_and = new QPushButton(centralWidget);
        btn_and->setObjectName(QStringLiteral("btn_and"));
        btn_and->setGeometry(QRect(410, 10, 80, 31));
        btn_or = new QPushButton(centralWidget);
        btn_or->setObjectName(QStringLiteral("btn_or"));
        btn_or->setGeometry(QRect(410, 50, 80, 31));
        btn_xor = new QPushButton(centralWidget);
        btn_xor->setObjectName(QStringLiteral("btn_xor"));
        btn_xor->setGeometry(QRect(410, 90, 80, 31));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 510, 20));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Lab_5_ui_calc", Q_NULLPTR));
        result->setText(QApplication::translate("MainWindow", "=", Q_NULLPTR));
        clear->setText(QApplication::translate("MainWindow", "C", Q_NULLPTR));
        btn_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        btn_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        btn_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        btn_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        btn_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        btn_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        btn_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        btn_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        btn_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        btn_0->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        btn_plus->setText(QApplication::translate("MainWindow", "+", Q_NULLPTR));
        btn_minus->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        btn_multi->setText(QApplication::translate("MainWindow", "*", Q_NULLPTR));
        btn_divide->setText(QApplication::translate("MainWindow", "/", Q_NULLPTR));
        btn_dot->setText(QApplication::translate("MainWindow", ".", Q_NULLPTR));
        btn_and->setText(QApplication::translate("MainWindow", "&&", Q_NULLPTR));
        btn_or->setText(QApplication::translate("MainWindow", "|", Q_NULLPTR));
        btn_xor->setText(QApplication::translate("MainWindow", "^", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
