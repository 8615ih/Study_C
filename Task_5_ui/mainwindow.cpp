#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <string>
#include <bitset>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sDisplay = "";
    cAction = ' ';
    sValue1 = sValue2 = "";

//=================================|signal/slot|=====================================//
// Связываем слоты с сигналами кнопок
    connect(ui->btn_0, SIGNAL(clicked()), this, SLOT(btn_0_clicked()));
    connect(ui->btn_1, SIGNAL(clicked()), this, SLOT(btn_1_clicked()));
    connect(ui->btn_2, SIGNAL(clicked()), this, SLOT(btn_2_clicked()));
    connect(ui->btn_3, SIGNAL(clicked()), this, SLOT(btn_3_clicked()));
    connect(ui->btn_4, SIGNAL(clicked()), this, SLOT(btn_4_clicked()));
    connect(ui->btn_5, SIGNAL(clicked()), this, SLOT(btn_5_clicked()));
    connect(ui->btn_6, SIGNAL(clicked()), this, SLOT(btn_6_clicked()));
    connect(ui->btn_7, SIGNAL(clicked()), this, SLOT(btn_7_clicked()));
    connect(ui->btn_8, SIGNAL(clicked()), this, SLOT(btn_8_clicked()));
    connect(ui->btn_9, SIGNAL(clicked()), this, SLOT(btn_9_clicked()));
    connect(ui->btn_dot, SIGNAL(clicked()), this, SLOT(btn_dot_clicked()));
    connect(ui->btn_plus, SIGNAL(clicked()), this, SLOT(btn_plus_clicked()));
    connect(ui->btn_minus, SIGNAL(clicked()), this, SLOT(btn_minus_clicked()));
    connect(ui->btn_multi, SIGNAL(clicked()), this, SLOT(btn_multi_clicked()));
    connect(ui->btn_divide, SIGNAL(clicked()), this, SLOT(btn_divide_clicked()));
    connect(ui->btn_and, SIGNAL(clicked()), this, SLOT(btn_and_clicked()));
    connect(ui->btn_or, SIGNAL(clicked()), this, SLOT(btn_or_clicked()));
    connect(ui->btn_xor, SIGNAL(clicked()), this, SLOT(btn_xor_clicked()));
    connect(ui->clear, SIGNAL(clicked()), this, SLOT(clear_clicked()));
    connect(ui->result, SIGNAL(clicked()), this, SLOT(result_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//=================================|buttons|=====================================//
void MainWindow::btn_0_clicked()
{
    sDisplay += "0";
    sValue2 += "0";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_1_clicked()
{
    sDisplay += "1";
    sValue2 += "1";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_2_clicked()
{
    sDisplay += "2";
    sValue2 += "2";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_3_clicked()
{
    sDisplay += "3";
    sValue2 += "3";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_4_clicked()
{
    sDisplay += "4";
    sValue2 += "4";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_5_clicked()
{
    sDisplay += "5";
    sValue2 += "5";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_6_clicked()
{
    sDisplay += "6";
    sValue2 += "6";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_7_clicked()
{
    sDisplay += "7";
    sValue2 += "7";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_8_clicked()
{
    sDisplay += "8";
    sValue2 += "8";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_9_clicked()
{
    sDisplay += "9";
    sValue2 += "9";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_dot_clicked()
{
    sDisplay += ".";
    sValue2 += ".";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_minus_clicked()
{
    cAction = '-';
    sValue1 = sDisplay;
    sDisplay += "-";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_plus_clicked()
{
    cAction = '+';
    sValue1 = sDisplay;
    sDisplay += "+";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_multi_clicked()
{
    cAction = '*';
    sValue1 = sDisplay;
    sDisplay += "*";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_divide_clicked()
{
    cAction = '/';
    sValue1 = sDisplay;
    sDisplay += "/";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_and_clicked()
{
    cAction = '&';
    sValue1 = sDisplay;
    sDisplay += "&";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_or_clicked()
{
    cAction = '|';
    sValue1 = sDisplay;
    sDisplay += "|";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::btn_xor_clicked()
{
    cAction = '^';
    sValue1 = sDisplay;
    sDisplay += "^";
    sValue2 = "";
    ui->lineEdit->setText(sDisplay);
}

void MainWindow::clear_clicked()
{
    sDisplay = "";
    ui->lineEdit->setText("");
    sValue1 = "";
    sValue2 = "";
}

//===================================|calculations|===============================================//
void MainWindow::result_clicked()
{
    double dValue1 = sValue1.toDouble();
    double dValue2 = sValue2.toDouble();
    double dResult = 0;
    unsigned int bResult = 0;
    // выявляем какую операцию необходимо выполнить
    if (cAction == '+')
    {
    dResult = dValue1 + dValue2;
    }
    if (cAction == '-')
    {
    dResult = dValue1 - dValue2;
    }
    if (cAction == '*')
    {
    dResult = dValue1 * dValue2;
    }
    if (cAction == '/')
    {
    dResult = dValue1 / dValue2;
    }
    if (cAction == '&')
    {
        int dValue1 = sValue1.toInt();
        int dValue2 = sValue2.toInt();
  //      std::string binary1 = std::bitset<8>(dValue1).to_string();
  //      std::string binary2 = std::bitset<8>(dValue2).to_string();
        bResult = dValue1 & dValue2;
        dResult = bResult;
    }
     if (cAction == '|')
    {
        int dValue1 = sValue1.toInt();
        int dValue2 = sValue2.toInt();
        bResult = dValue1 | dValue2;
        dResult = bResult;
    }
     if (cAction == '^')
    {
        int dValue1 = sValue1.toInt();
        int dValue2 = sValue2.toInt();
        bResult = dValue1 ^ dValue2;
        dResult = bResult;
    }
        sDisplay += "=" + QString::number(dResult);
        ui->lineEdit->setText(sDisplay);
}




