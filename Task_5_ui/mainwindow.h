#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void btn_1_clicked();

    void btn_2_clicked();

    void btn_3_clicked();

    void btn_4_clicked();

    void btn_5_clicked();

    void btn_6_clicked();

    void btn_7_clicked();

    void btn_8_clicked();

    void btn_9_clicked();

    void btn_dot_clicked();

    void btn_minus_clicked();

    void btn_plus_clicked();

    void btn_multi_clicked();

    void btn_divide_clicked();

    void btn_and_clicked();

    void btn_or_clicked();

    void btn_xor_clicked();

    void clear_clicked();

    void result_clicked();

    void btn_0_clicked();







private:
    Ui::MainWindow *ui;
    QString sDisplay; // переменная типа QString для хранения строки вывода
    QString sValue1, sValue2;
    char cAction; // переменная для хранения вычисленияоперации вычисления
};

#endif // MAINWINDOW_H
