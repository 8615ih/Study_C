// 'Hello World!' program
/*
* Лабораторная работа No1
* Исследование комментариев С++:
* This program just displays a string and exits
* Developer: Igor Khizhinsky
*/

/*============================|LIBRARIES|==============================================*/
#include <iostream> /* Подключаем библиотеку для ввода/вывода */
#include <QCoreApplication> /* Подключаем библиотеку QCoreApplication */
#include <stdio.h> /* Подключаем библиотеку для printf */
#include <iomanip> /* Подключаем библиотеку для манипуляторов */
 using namespace std;
/*============================|VARIABLES|=============================================*/
// Глобальные переменные


/*============================|ENTRANCE|==============================================*/

int main() //Главная функция с которой начинается исполнение программы
{/* Закрытая скобка - начало блока стейтмена, кода относящийся к функции main */
// TODO: Тело функции

/*=================================|Task_1|===================================================*/
  std::cout << "*****\t\t\t\t   ТУСУР   \t\t\t\t*****" << std::endl;
  printf("\n");
  printf("\n");
  std::cout << setw(80) << std::right << "Faculty: Additional education" << std::endl;
  std::cout << setw(61) << std::right << "Group: 310" << std::endl;
  std::cout << setw(77) << std::right << "Developer: Igor Khizhinsky" << std::endl;
  std::cout << setw(69) << std::right << "Laboratory work: 2" << std::endl;
  printf("\n");
  std::cout << "\t\t\t\t   Томск 2023   \t\t\t\t" << std::endl;
  printf("\n");

/*=================================|Directive comments|===================================================*/
#if 0
    Directive comments
#endif
#if 0
    Подключаем библиотеку для ввода/вывода
#endif
/*=================================|Task_2|===================================================*/
  std::cout << "****\t\t\t\t Типы данных \t\t\t\t*****" << std::endl; // вывод сообщения
  printf("\n");
  std::cout << "Определение размеров основных типов:" << std::endl; // вывод сообщения

// Инициализация переменных
  int    valInt    = 45;   // объявление переменной a целого типа.
  float  valFloat  = 3.15; // объявление переменной b типа данных с плавающей запятой.
  double valDouble = 14.2; // инициализация переменной типа double.
  char   valChar   = 'c';  // инициализация переменной типа char.
  bool   valBool   = true; // инициализация логической переменной k.


// Определение размеров основных типов
  std::cout << "int:" << sizeof(valInt) << " bytes;" << std::endl; // вывод длины в байтах типа int
  std::cout << "bool:" << sizeof(valBool) << " bytes;" << std::endl; // вывод длины в байтах типа bool
  std::cout << "float:" << sizeof(valFloat) << " bytes;" << std::endl; // вывод длины в байтах типа float
  std::cout << "Char:" << sizeof(valChar) << " bytes;" << std::endl; // вывод длины в байтах типа char
  std::cout << "double:" << sizeof(valDouble) << " bytes;" << std::endl; // вывод длины в байтах типа Double

  printf("\n");
  std::cout << "Определение размеров основных типов с использованием модификаторов:" << std::endl; // вывод сообщения

// С использованием модификаторов длины
  signed long int  valSigInt     = -33;
  unsigned int     valUnsInt     = 33;
  long int         valLongInt    = 125;
  short int        valShortInt   = 33;
  long long int    valLlongInt   = 1033;
  signed char      valSigChar    = 'X';
  unsigned char    valUnsChar    = 'Y';
  long double      valLongDouble = 3.14;

// Определение размеров основных типов с использованием модификаторов
  std::cout << "signed long int:" << sizeof(valSigInt) << " bytes;" << std::endl;
  std::cout << "unsigned int:" << sizeof(valUnsInt) << " bytes;" << std::endl;
  std::cout << "long int:" << sizeof(valLongInt) << " bytes;" << std::endl;
  std::cout << "short int:" << sizeof(valShortInt) << " bytes;" << std::endl;
  std::cout << "long long int:" << sizeof(valLlongInt) << " bytes;" << std::endl;
  std::cout << "signed char:" << sizeof(valSigChar) << " bytes." << std::endl;
  std::cout << "unsigned char:" << sizeof(valUnsChar) << " bytes." << std::endl;
  std::cout << "long double:" << sizeof(valLongDouble) << " bytes." << std::endl;
  printf("\n");

/*=================================|Task_3|===================================================*/
  std::cout << "Расчитываем средднее арифметическое значение:" << std::endl;
  printf("\n");

  float input1, input2, input3;
  float output;

  std::cout << "Введите первое целое число:" << std::endl;
  cin >> input1;
  std::cout << "Введите второк целое число:" << std::endl;
  cin >> input2;
  std::cout << "Введите третье целое число:" << std::endl;
  cin >> input3;
  output = (input1 + input2 + input3)/3;
  std::cout << "Результат:" << output << std::endl;
  printf("\n");

/*=================================|Task_4|===================================================*/
  std::cout << "Решаем задачи:" << std::endl;
  printf("\n");
  cout << "1. Вычиляем скорость прямолинейного движения:" << endl;
  printf("\n");
  float S, time;
  float speed;

  std::cout << "Введите значение расстояния S (км):" << std::endl;
  cin >> S;
  std::cout << "Введите значение времени t (часов):" << std::endl;
  cin >> time;
  speed = S/time;
  std::cout << "Ответ:" << speed << " км/ч"<< std::endl;
  printf("\n");

  cout << "2. Вычиляем ускорение:" << endl;
  printf("\n");
  float V1, V2;
  float accel;
  float t;

  std::cout << "Введите значение насальной скорости V1 (м/с):" << std::endl;
  cin >> V1;
  std::cout << "Введите значение конечной скорости V2 (м/с):" << std::endl;
  cin >> V2;
  std::cout << "Введите значение времени t (сек):" << std::endl;
  cin >> t;
  accel = (V2 - V1)/(t);
  std::cout << "Ответ:" << accel << " м/с^2"<< std::endl;
  printf("\n");

  cout << "3. Вычиляем радиус круга:" << endl;
  printf("\n");
  float C, R;
  float pi = 3.14;

  std::cout << "Введите значение длины окружности С :" << std::endl;
  cin >> C;
  R = C/(2*pi);
  std::cout << "Ответ:" << R << std::endl;
  printf("\n");

  /*=================================|Task_4|===================================================*/
  std::cout << "Перевод в экспоненциальную форму:" << std::endl;
  printf("\n");

  double n1 = 34.50;
  double n2 = 0.00400;
  double n3 = 123.005;
  double n4 = 146000;

  cout.setf(ios::scientific);
  cout << "N1=" << n1 << endl;
  cout << "N2=" << n2 << endl;
  cout << "N3=" << n3 << endl;
  cout << "N4=" << n4 << endl;

/* Закрытая скобка - окончание блока стейтмена, кода относящийся к функции main */
  return 0; // Возвращаемое значение
}/* Закрытая скобка - окончание блока стейтмена, кода относящийся к функции main */

/*================================================================================*/
// Heppy end!
/*================================================================================*/
