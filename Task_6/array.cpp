#include <random>
#include <iostream>
#include <algorithm>

int iArray()
{
    std::random_device rd;
    std::mt19937 mersenne(rd());

    int i_array[10];
// заполняем массив
    for (int i = 0; i < 10; ++i)
    {
        i_array[i] = mersenne();
    }
// выводим содержимое маасива
   std::cout << "\nOur array: ";
   std::sort(i_array, i_array+10);       // сортировка массива по возрастанию

    for (int i = 0; i < 10; ++i)
    {
        std::cout << i_array[i] << " ";
    }

    std::cout << std::endl;

// поиск min и max значений
//  int INT_MAX, INT_MIN;                // для Linux
    int min = INT_MAX, max = INT_MIN;
    for (int i: i_array)
    {
        if (i < min)
        {
            min = i;
        }

        if (i > max)
        {
            max = i;
        }
    }

     std::cout << "The min element is " << min << std::endl;
     std::cout << "The max element is " << max << std::endl;

return 0;

}
