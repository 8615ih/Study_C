#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string>

using namespace std;

int stingIO()
{
/*=================================|Task_1|===================================================*/
  std::string university;
  std::string faculty;
  std::string fullName;
  std::string city;
  std::string group;
  std::string year;

  std::cout << "Enter the name of the university: ";
  std::getline(std::cin, university);
  std::cout << "Enter the name of the faculty: ";
  std::getline(std::cin, faculty);
  std::cout << "Enter group number: ";
  std::getline(std::cin, group);
  std::cout << "Enter your full name: ";
  std::getline(std::cin, fullName);
  std::cout << "Enter the city: ";
  std::getline(std::cin, city);
  std::cout << "Enter year: ";
  std::getline(std::cin, year);


  std::cout << "*****\t\t\t\t " <<  university <<  " \t\t\t\t*****" << std::endl;
  printf("\n");
  printf("\n");
  std::cout  << "Faculty: " << faculty << std::endl;
  std::cout  << "Group: " << group << std::endl;
  std::cout  << "Developer: " << fullName << std::endl;
//std::cout << setw(69) << std::right << "Laboratory work: 2" << std::endl;
  printf("\n");
  std::cout << "\t\t\t\t "  << city << ' ' << year <<  " \t\t\t\t" << std::endl;
  printf("\n");
return 0;
}
