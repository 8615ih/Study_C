#include <iostream>

int n;
int i;
int res;

using namespace std;

int iteration()
{
  int n;
  int i;
  int res;

  cout << "Selected method iteration" << endl;
  cout << "Enter the number: ";
  cin >> n;
  res = 1;
  for (i = 1; i <= n; i++) {
    res = res * i;
  }
  cout << "Result: " << res;
}

//=================================================//

int recursion(int i)
{
  if (i==0) return 1;
  else return i*recursion(i-1);
}



int factorial()
{
    int method;
    std::cout << "Factorial of a number" << std::endl;
    std::cout << "Choose a method (1-iteration, 2-recursion): ";
    std::cin >> method;

    if (method == 1)
    {
    iteration();
    }
    else
    {
    cout << "Selected method recursion" << endl;
    cout << "Enter the number: ";
    cin >> n;
    cout << "Result: " << recursion(n);
     }
}
