#include <iostream>

int matrix()
{
    int array[5][5] =
    {
        { 1 },             // строка №0 = 1, 0, 0, 0, 0
        { 0, 1 },          // строка №1 = 0, 1, 0, 0, 0
        { 0, 0, 1 },       // строка №2 = 0, 0, 1, 0, 0
        { 0, 0, 0, 1 },    // строка №3 = 0, 0, 0, 1, 0
        { 0, 0, 0, 0, 1 }, // строка №4 = 0, 0, 0, 0, 1
     };

    std::cout << " " << "0-column " << "1-column " << "2-column " << "3-column " << "4-column " << '\n';
    std::cout << "0 line: " << array[0][0] << "\t" << array[0][1] << "\t" << array[0][2] << "\t" << array[0][3] << "\t" << array[0][4] << '\n';
    std::cout << "1 line: " << array[1][0] << "\t" << array[1][1] << "\t" << array[1][2] << "\t" << array[1][3] << "\t" << array[1][4] << '\n';
    std::cout << "2 line: " << array[2][0] << "\t" << array[2][1] << "\t" << array[2][2] << "\t" << array[2][3] << "\t" << array[2][4] << '\n';
    std::cout << "3 line: " << array[3][0] << "\t" << array[3][1] << "\t" << array[3][2] << "\t" << array[3][3] << "\t" << array[3][4] << '\n';
    std::cout << "4 line: " << array[4][0] << "\t" << array[4][1] << "\t" << array[4][2] << "\t" << array[4][3] << "\t" << array[4][4] << '\n';

    return 0;
}
