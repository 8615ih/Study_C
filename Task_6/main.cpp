/*
* Лабораторная работа No6
* Developer: Igor Khizhinsky
*/

/*============================|LIBRARIES|==============================================*/
#include <iostream> /* Подключаем библиотеку для ввода/вывода */
#include <QCoreApplication> /* Подключаем библиотеку QCoreApplication */
#include <stdio.h> /* Подключаем библиотеку для printf */
#include <iomanip> /* Подключаем библиотеку для манипуляторов */
#include "main.h"

/* Укажите необходимый номер UNIT-а:
 * Номер юнита соответсвует номеру задания.
*/
#define UNIT 2

 using namespace std;
/*============================|VARIABLES|=============================================*/
// Глобальные переменные


/*============================|ENTRANCE|==============================================*/

int main() //Главная функция с которой начинается исполнение программы
{
    #if UNIT == 1
        stingIO();
    #elif UNIT == 2
        iArray();
    #elif UNIT == 3
        matrix();
    #elif UNIT == 4
        matrix8x8();
    #elif UNIT == 5
        ascii();
    #elif UNIT == 6
        factorial();
    #elif UNIT == 7
        fibnacci();
    #else
        pyramid();
    #endif
return 0;
}
