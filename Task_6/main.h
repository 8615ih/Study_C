#ifndef MAIN_H
#define MAIN_H

int stingIO();
int iArray();
int matrix();
int matrix8x8();
void ascii();
int factorial();
int fibnacci();
int pyramid();

#endif // MAIN_H
