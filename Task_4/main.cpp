/*
* Лабораторная работа No4
* Исследование комментариев С++:
* This program just displays a string and exits
* Developer: Igor Khizhinsky
*/

/*============================|LIBRARIES|==============================================*/
#include <iostream> /* Подключаем библиотеку для ввода/вывода */
#include <QCoreApplication> /* Подключаем библиотеку QCoreApplication */
#include <stdio.h> /* Подключаем библиотеку для printf */
#include <iomanip> /* Подключаем библиотеку для манипуляторов */

using namespace std;

#define logic4 true

/*============================|ENTRANCE|==============================================*/
int main()
{
    std::cout << "*****\t\t\t\t   ТУСУР   \t\t\t\t*****" << std::endl;
    printf("\n");
    printf("\n");
    std::cout << setw(80) << std::right << "Faculty: Additional education" << std::endl;
    std::cout << setw(61) << std::right << "Group: 310" << std::endl;
    std::cout << setw(77) << std::right << "Developer: Igor Khizhinsky" << std::endl;
    std::cout << setw(69) << std::right << "Laboratory work: 4" << std::endl;
    printf("\n");
    std::cout << "\t\t\t\t   Томск 2023   \t\t\t\t" << std::endl;
    printf("\n");

//============================|True or False?|=====================================//
    std::cout << "1. True or False?" << std::endl;
    printf("\n");

    std::cout << "a. (true && true)||false ?" << std::endl;
    if (((true && true)||false) == 1)
       std::cout << "true" << std::endl;
    else
       std::cout << "false" << std::endl;
    printf("\n");

    std::cout << "b. (false && true)||true ?" << std::endl;
    if (((false && true)||true) == 1)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
    printf("\n");

    std::cout << "c. (false && true)||false||true ?" << std::endl;
    if (((false && true)||false||true) == 1)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
    printf("\n");

    std::cout << "(5 > 6 || 4 > 3) && (7 > 8) ?" << std::endl;
    if (((5 > 6 || 4 > 3) && (7 > 8)) == 1)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
    printf("\n");

    std::cout << "!(7 > 6 || 3 > 4) ?" << std::endl;
    if (!(7 > 6 || 3 > 4) == 1)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
    printf("\n");

//=============================|Const, Constexpr, Macro-obj define and enum|=======================//
    const     bool logic1(true);
    constexpr bool logic2(true);
    constexpr bool logic3(false);
    const     bool logic5(false);
    constexpr bool logic6(true);

    std::cout << "2. Const, Constexpr, Macro-obj define and enum" << std::endl;
    std::cout << "a. Const, Constexpr, Macro-obj define" << std::endl;
    printf("\n");
    std::cout <<  std::boolalpha << "(" << logic1 << " && " << logic2 << ") || (!" << logic3 << ") && (" << logic4 << " || " << logic5 << ") ?" << std::endl;
    std::cout << ((logic1 && logic2) || (!logic3) && (logic4 || logic5)) << std::endl;
    std::cout << "(" << logic1 << " && " << logic2 << ") || (" << logic3 << " && " << logic4 << ") || (!" << logic5 << ") ?" << std::endl;
    std::cout << ((logic1 && logic2) || (logic3 && logic4) || (!logic5)) << std::endl;
    std::cout << "(" << logic1 << " || " << logic2 << ") && (" << logic3  << " || " << logic4 << ") && (" << logic5 << " || " << logic6 << ") ?" << std::endl;
    std::cout << ((logic1 || logic2) && (logic3 || logic4) && (logic5 || logic6)) << std::endl;
    printf("\n");

    enum logicEnum
    {
        logicEnum1 = 254,
        logicEnum2 = 512,
        logicEnum3 = 1024,
        logicEnum4 = 2048,
        logicEnum5 = 4096,
        logicEnum6 = 8192
    };
    std::cout << "b. Enum" << std::endl;
    printf("\n");
    std::cout << "(" << logicEnum1 << " > " << logicEnum2 << ") && (" << logicEnum3 << " < " << logicEnum4 << ") && (" << logicEnum5 << " != " << logicEnum6 << ") ?" << std::endl;
    std::cout << ((logicEnum1 > logicEnum2) && (logicEnum3 < logicEnum4) && (logicEnum5 != logicEnum6)) << std::endl;
    printf("\n");

//======================================|de Morgan's law|=========================================//
    bool a, b;

    std::cout << "3. de Morgan's first law" << std::endl;
    std::cout << "((!(a && b)) == (!a) || (!b))" << std::endl;
    std::cout << "Введите логическую переменную a (0 или 1):" << std::endl;
    std::cin >> a;
    std::cout << "Введите логическую переменную b (0 или 1):" << std::endl;
    std::cin >> b;
    std::cout << "(!(" << a << " && " << b << ")) == (!" << a << ") || (! " << b << "))" << std::endl;
    if ((!(a && b)) == (!a) || (!b))
        std::cout << "de Morgan's first law confirmed" << std::endl;
    else
        std::cout << "de Morgan's first law has not been confirmed" << std::endl;
    printf("\n");

    std::cout << "de Morgan's second law" << std::endl;
    std::cout << "((!(a || b)) == (!a) && (!b))" << std::endl;
    std::cout << "Введите логическую переменную a (0 или 1):" << std::endl;
    std::cin >> a;
    std::cout << "Введите логическую переменную b (0 или 1):" << std::endl;
    std::cin >> b;
    std::cout << "(!(" << a << " || " << b << ")) == (!" << a << ") && (! " << b << "))" << std::endl;
    if ((!(a || b)) == (!a) && (!b))
        std::cout << "de Morgan's second law confirmed" << std::endl;
    else
        std::cout << "de Morgan's second law has not been confirmed" << std::endl;
    printf("\n");

//======================================|logicValue|=========================================//
    int x,y,z,v;
    bool logicValue;

    std::cout << "4. logicValue" << std::endl;
    std::cout << "Введите целочисленную переменную x:" << std::endl;
    std::cin >> x;
    std::cout << "Введите целочисленную переменную y:" << std::endl;
    std::cin >> y;
    std::cout << "Введите целочисленную переменную z:" << std::endl;
    std::cin >> z;
    std::cout << "Введите целочисленную переменную v:" << std::endl;
    std::cin >> v;
    printf("\n");


    x  = 3 + 4 + 5;   //сложение
    std::cout << "x = 3 + 4 + 5" << std::endl;
    std::cout << "x:" << x << std::endl;
    std::cout << "y:" << y << std::endl;
    std::cout << "z:" << z << std::endl;
    printf("\n");


    x  = y = z;       //присваивание
    std::cout << "x = y = z" << std::endl;
    std::cout << "x:" << x << std::endl;
    std::cout << "y:" << y << std::endl;
    std::cout << "z:" << z << std::endl;
    printf("\n");


    z *= ++y + 5;     //пре-инкреент + 5 и умножение с присваиванием
    std::cout << "z *= ++y + 5" << std::endl;
    std::cout << "x:" << x << std::endl;
    std::cout << "y:" << y << std::endl;
    std::cout << "z:" << z << std::endl;
    printf("\n");


    logicValue = x || y && z || v;
    std::cout << "x || y && z || v" << std::endl;         // x=0, y=0, z=0, v=0  -> false
    std::cout << "logicValue:" << logicValue << std::endl;
    printf("\n");


    return 0;
}
