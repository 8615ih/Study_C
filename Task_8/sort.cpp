#include <iostream>
#include <array>
#include <algorithm> // для std::sort

int sort()
{
std::array<int, 5> myarray { 8, 4, 2, 7, 1 };

std::sort(myarray.begin(), myarray.end()); // сортировка массива по возрастанию
for (const auto &element : myarray)
{
std::cout << element << ' ';
}
std::cout << "\n";
std::sort(myarray.rbegin(), myarray.rend()); // сортировка массива по убыванию
for (const auto &element : myarray)
{
std::cout << element << ' ';
}

return 0;
}
