#include <iostream>
#include <list>
#include <numeric>


template<typename T>

double getAverage(std::list<T> const& numbers) {
    if (numbers.empty()) {
        return 0;
    }

    double sum = 0.0;
    for (const T &i: numbers) {
        sum += (double)i;
    }
    return sum / numbers.size();
}

int list()
{
std::list<int> numbers = { 1, 2, 3, 4, 5 };
std::cout << "Current list: " << "\t";
for (auto iter = numbers.begin(); iter != numbers.end(); iter++) {
std::cout << *iter << "\t";
}
std::cout << "\n";
double avg = getAverage(numbers);
std::cout << "Average: " << avg << std::endl;

numbers.push_back(avg); // { 1, 2, 3, 4, 5, 23 }
std::cout << "List + average: " << "\t";
for (auto iter = numbers.begin(); iter != numbers.end(); iter++) {
std::cout << *iter << "\t";
}

return 0;
}
