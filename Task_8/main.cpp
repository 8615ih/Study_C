#include <iostream>
#include "main.h"
#include <functional>

#define UNIT 3

int main()
{
// Указатели на функции
    std::function<int()> fcn1;
    std::function<int()> fcn2;
    std::function<int()> fcn3;
    std::function<int()> fcn4;
    std::function<int()> fcn5;
    std::function<int()> fcn6;

// aliases
    fcn1 = vector;
    fcn2 = sort;
    fcn3 = vectorList;
    fcn4 = list;
    fcn5 = pair;

#if (UNIT == 1)
    fcn1();
#elif (UNIT == 2)
    fcn2();
#elif (UNIT == 3)
    fcn3();
#elif (UNIT == 4)
    fcn4();
#elif (UNIT == 5)
    fcn5();
#else
    std::cout << "Unknown" << std::endl;
#endif

    return 0;
}
