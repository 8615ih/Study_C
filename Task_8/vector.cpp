#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm> // для std::sort
#include <string>
#include <iterator>



// функция генерации псевдослучайных чисел
int getRandomNumber(int min, int max)
{
static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
// Равномерно распределяем рандомное число в нашем диапазоне
return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

int vector()
{
    int N;
    std::cout << "N (1 to 1000):" << std::endl;
    std::cin >> N;

    std::vector<int> myVector; // объявляем массив типа int длиной 4
    myVector.resize(N);
    std::cout << "The length is: " << myVector.size() << '\n';

    for (int i = 0; i < N; ++i)
    {
        myVector[i] = getRandomNumber(-1000, 1000);  // заполняем метрицу
        std::cout << myVector[i] << "\t";        // выводим значение

        if ((i+1) % 10 == 0)
        std::cout << "\n";


    }

    std::cout << "\n";
    std::cout << "=============================================================================" << std::endl;
    bool temp = true;
    int count = 0;
    for ( int i = 0; i < N; i ++ )
        {
            for ( int j = 0; j < N; j ++ )
            {
                if ( myVector[i] == myVector[j] && i != j ) // остановка если элементы совпадают
                {
                    temp = false;
                    break; // не уникально
                }

            }
            ++count;
            //if ( temp ) std::cout << " " << myVector[i];
            temp = true; // уникально
        }
      std::cout << "\n";
      std::cout << "Uniq_count: " << count << "\n";
return 0;
}





